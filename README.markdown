---
labels:
- 'Stage-Alpha'
summary: 'Restrict login by Chain of Endorsement'
---

Introduction
============

This is an experimental access control mod that limits login rights on a host
to admins, friends of admins, friends of friends, etc.  Users can "ensorse" or
"censure" each other, creating a mechanism for distributed moderation of the
server.

This mod should help a relatively small, and/or largely absentee, team of
admins to manage a large community of users, by delegating authority and
accountability to a larger team of moderators, who can then delegate to more
members, and so on as the community grows.

Details
=======

This mod only controls login access, and does not enable or restrict registrations.
It is intended that users should be able to register themselves (i.e. so they can
pick a username and password privately), but need to have an endorsemen from the
existing community before they can participate.

To log in, a user must have an unbroken chain of endorsements, originating at an
admin, i.e. be endorsed by someone endorsed by etc. an admin.  Multiple chains may
exist, but only one is necessary.

Currently, an "endorsement" is a presence subscription request (i.e. you endorse
a user by adding them to your roster).  Pending subscriptions are also valid.
Endorsement can be withdrawn by removing a user from your roster.  This may be
subject to change in future versions.

Users can also "censure" another user by blocking (i.e. via privacy lists).  This
breaks any chain of endorsement of (or through) that user that depend on the
blocking user.

Note that the community must exist entirely within a single hostname; chains of
endorsement cannot be traced across hostname boundaries

Examples
========

A is an admin, and invites friends B and C to participate.  B and C register,
and A adds them to his roster to allow them to log in.

B invites, and endorses, friends D and E.  D invites and endorses F and G.

If F turns out to be a spammer, or other user unwanted by the community, then
D can withdraw endorsement to ban him.  B or A can also block that user to
break his chain of endorsements and kick him out.

If F gets an additional endorsement from C, then this can overcome a block
by B, because the chain from A to C to F is not dependent on B.  However, since
A is the only admin at the root of all chains, nobody can override a block by
A; in this setup, it's effectively a ban.

F gets kicked out, but registers another account as H, and gets another
endorsement from D.  If this continues to happen, and D keeps endorsing this
user's accounts to avoid the ban, then B may withdraw endorsement of D, and/or
A or B can block D, which will remove D and everyone depending on D for
endorsement.  If G is a good member of the community, hopefully she will have
gathered additional endorsements and can remain, but otherwise, may lose access
along with D.

Social Contract
===============

In effect, a user who endorses another user bears some responsibility for that
user's behavior.  Endorsing users also have a responsibility to the users that
they endorse to remain in good standing with the community, to avoid losing
access for other users who depend on them.

Known Issues
============

Endorsements are currently very coarse-grained; a user is either a full
participant, able to log in, chat, invite and endorse other users, and
delegate authority to them, or cannot participate at all; there is no way to
grant a user selected rights, e.g. to log in and chat but not to invite or
endorse.  It is not known whether this fine-grained level of control is
necessary.

Endorsement/censure is managed via subscriptions and blocks, rather than a
more precise mechanism like adhocs, due to universality of support on clients,
but this means that you cannot e.g. subscribe to a user's presence without
also endorsing that user.

There is no built-in way for users to see the endorsement graph, so it may be
difficult to determine who is responsible for inviting an unwanted user.  This
may become relevant to censure a user who repeatedly invites unwanted users
to join (e.g. D in the example above).

Configuration
=============

There is nothing to configure.  The module takes effect and restricts logins on
any server/hostname for which it's enabled.  Other mods that restrict access may
also be used, but users must be accepted by all access control measures to
log in.

Compatibility
=============

Should work with 0.9+.
